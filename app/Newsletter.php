<?php

/**
*	@Autor 	Deyvison Estevam  <deyvisonestevam@gmail.com>
*	@dat 	04/04/2018
*	
*		
*/

namespace App;

use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
    protected $fillable = ['categoria', 'titulo', 'imagem', 'corpo', 'data_publicacao'];   

    protected $dates = ['deleted_at'];
   
}
