<?php

/**
*	@Autor 	Deyvison Estevam  <deyvisonestevam@gmail.com>
*	@dat 	04/04/2018
*	
*		
*/

namespace App\Http\Controllers;

use App\Newsletter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class NewslettersController extends Controller
{

    //Função responsável por validar o create/update
    protected function newsletterValidator($request) {
        $validator = Validator::make($request->all(), [
            'categoria' => 'required|max:100',
            'titulo' => 'required|max:60',
            'imagem'  => 'required|max:100',
            'corpo'     =>  'required',
            'data_publicacao'   =>  'required',
        ]);

        return $validator;
    }
    
	//Retorna todas as notícias
	public function index()
	{
		$newsletters = Newsletter::all();
		return response()->json($newsletters);
	}

	//Retorna um notícia específica
	public function show($id)
    {
        $newsletter = Newsletter::find($id);

        if(!$newsletter) {
            return response()->json([
                'message'   => 'Registro Não Encontrado',
            ], 404);
        }

        return response()->json($newsletter);
    }


    //Armazena uma notícia se for válida
    public function store(Request $request)
    {

        $validator = $this->newsletterValidator($request);
        if($validator->fails() ) {
            return response()->json([
                'message'   => 'Validation Failed',
                'errors'        => $validator->errors()
            ], 422);
        }
        
        $newsletter = new Newsletter();
        $newsletter->fill($request->all());
        $newsletter->save();

        return response()->json($newsletter, 201);
    }

    //Atualiza uma notícia se for válida
    public function update(Request $request, $id)
    {           

        $newsletter = Newsletter::find($id);

        if(!$newsletter) {
            return response()->json([
                'message'   => 'Registro Não Encontrado',
            ], 404);
        }

        $newsletter->fill($request->all());
        $newsletter->save();

        return response()->json($newsletter);
    }

    //Deletando uma notícia
    public function destroy($id)
    {
        $newsletter = Newsletter::find($id);

        if(!$newsletter) {
            return response()->json([
                'message'   => 'Registro Não Encontrado',
            ], 404);
        }else if($newsletter->delete()){
        	return response()->json([
                'message'   => 'Registro Deletado',
            ], 200);
        }else{
        	return response()->json([
                'message'   => 'Falha na Requisição',
            ], 404);
        }
    }
}
