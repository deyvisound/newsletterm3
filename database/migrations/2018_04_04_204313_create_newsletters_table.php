<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewslettersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('newsletters', function (Blueprint $table) {  
            $table->increments('id');
            $table->string('categoria', 100);
            $table->string('titulo', 60);
            $table->string('imagem', 100);
            $table->string('galeria')->default('http://urldasfotos.com.br/galeria_da_noticia_X/');
            $table->text('corpo');
            $table->date('data_publicacao');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('newsletters');
    }
}
