<?php

/**
*	@Autor 	Deyvison Estevam  <deyvisonestevam@gmail.com>
*	@dat 	04/04/2018
*	
*	Gerando notícias aleatórias	
*/

use Illuminate\Database\Seeder;

class NewsletterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Newsletter::create([
            'categoria' => str_random(20),
            'titulo' => str_random(10),
            'imagem' => 'http://urldoservidor.'.str_random(10),
            'corpo' => str_random(100) . " " . str_random(100) . " " . str_random(100) . " " . str_random(100),
            'data_publicacao' => date('Y-m-d'),            
        ]);

        App\Newsletter::create([
            'categoria' => str_random(20),
            'titulo' => str_random(10),
            'imagem' => 'http://urldoservidor.'.str_random(10),
            'corpo' => str_random(100) . " " . str_random(100) . " " . str_random(100) . " " . str_random(100),
            'data_publicacao' => date('Y-m-d'),            
        ]);

        App\Newsletter::create([
            'categoria' => str_random(20),
            'titulo' => str_random(10),
            'imagem' => 'http://urldoservidor.'.str_random(10),
            'corpo' => str_random(100) . " " . str_random(100) . " " . str_random(100) . " " . str_random(100),
            'data_publicacao' => date('Y-m-d'),            
        ]);    
        
    }
}
