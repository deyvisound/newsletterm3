# newsletterM3

--Após o Dowload do projeto, execute o seguinte comando na raiz do mesmo:

composer install

--Após a instalação de todas as dependências crie o banco newsletter(ou outro nome qualquer) e realize a configuração do .env de acordo com seu banco de dados. Feito isso, execute os seguintes comandos:

--Populando a base de dados
> php artisan migrate --seed

--Populando as tabelas de autenticação oauth
> php artisan passport:install

--A API irá criar dois clientes(listagem abaixo), copie-os em um .txt para utilização posterior

##------------------------------------------------------
Client ID: 1
Client Secret: BIl3PjSJ2j6hZCzxJLBOiCVvHMfwTyl86BejnDlu
Password grant client created successfully.
Client ID: 2
Client Secret: gTAVAo5sy9O2hO4I6c9L2QO0o3jwUaU55qtSjU5C
##------------------------------------------------------


--Agora basta gerar a chave(php artisan key:generate) e subir o server(php artisan serve)

--Utilizei o PostMan(https://www.getpostman.com/) para testa a api, mas também funciona por cURL.

--Ao abrir o postman, basta importar a coleção presente na raiz do projeto: 'minha_colecao.postman_collection.json', lá estão presentes as requisições testadas e descritas em ordem de execução. Basta substituir a chave e token gerados.



--OBS:
>Nas vezes que foram testadas, apenas o cliente 2 funcionou. 
